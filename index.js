'use strict'

const port = process.env.PORT || 3300;

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pen'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');

const app = express();


var db = mongojs('mongodb+srv://MongoDBCloud:mongodb1234@cluster0.numor.mongodb.net/db-hoteles?retryWrites=true&w=majority');
//var db = mongojs('localhost:27017/Servicios'); // Conectamos con la BD
var id = mongojs.ObjectID;

//Declaramos los middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended:false}));
app.use(express.json());

app.param("colecciones", (req, res, next, colecciones) => {
    console.log('Middleware de tipo param /api/:colecciones');
    req.collection = db.collection(colecciones);
    return next();
});

// Declaramos nuestro middleware de autorización
function auth (req, res, next) {
    if (!req.headers.authorization) {
        res.status(401).json({
            result: 'KO',
            mensaje: "No se ha enviado el token en la cabecera token"
        });
        return next(new Error("Falta token de autorización"));
    }

    console.log(req.headers.authorization);
    if (req.headers.authorization.split(" ")[1] === "MITOKEN123456789") {
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio."
    });
    return next(new Error("Acceso no autorizado."));
}

// Declaramos nuestras rutas y nuestros controladores
app.get('/api', (req, res, next)=> {
    db.getCollectionNames((err, colecciones)=> {
        if (err) return next(err); // Llamamos al siguiente middleware propangando el error

        console.log(colecciones);
        res.json({
            result: "ok",
            colecciones: colecciones
        });
    });
});

app.get('/api/:colecciones', (req, res, next) => {
    const queColeccion = req.params.colecciones;

    req.collection.find((err, elementos) => { 
        if (err) return next(err);

        console.log(elementos);
        res.json({
            result: "ok",
            colecciones: queColeccion,
            elementos: elementos
        });
    });
});

app.get('/api/:colecciones/reserva', (req, res, next) => {
    const queColeccion = req.params.colecciones;


    req.collection.find({disponible: "True" }, (err, elementos) => { 

        if (err) return next(err);

        console.log(elementos);

        
        res.json({
            result: "ok",
            colecciones: queColeccion,
            elementos: elementos
        });
    });
});


app.get('/api/:colecciones/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queID = req.params.id;

    req.collection.findOne({ _id: id(queID) }, (err, elemento) => { 
        if (err) return next(err);

        console.log(elemento);
        res.json({
            result: "ok",
            colecciones: queColeccion,
            elemento: elemento
        });
    });
});

// app.post('/api/:colecciones', auth, (req, res, next) => {
//     const nuevoElemento = req.body;
//     const queColeccion = req.params.colecciones;

//     req.collection.save(nuevoElemento, (err, elementoGuardado)=> {
//         if (err) return next(err);

//         console.log(elementoGuardado);
//         res.status(201).json({
//             result: "ok",
//             colección: queColeccion,
//             elemento: elementoGuardado
//         });
//     });
// });


app.put('/api/:colecciones/reserva/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queID = req.params.id;
    const nuevosDatos = req.body;

    req.collection.findAndModify({
        query: { _id: id(queID) },
        update: {$set: {disponible: "False"}}},
        (err, resultado) => {
            if (err) return next(err);

            console.log(resultado);
            res.json({
                result: "ok",
                coleccion: queColeccion,
                elemento: queID,
                resultado: resultado
            });
        }
    );
});

app.post('/api/:colecciones', (req, res, next) => {
    const nuevoElemento = req.body;
    const queColeccion = req.params.colecciones;

    req.collection.save(nuevoElemento, (err, elementoGuardado)=> {
        if (err) return next(err);

        console.log(elementoGuardado);
        res.status(201).json({
            result: "ok",
            colección: queColeccion,
            elemento: elementoGuardado
        });
    });
});

// app.put('/api/:colecciones/:id', auth, (req, res, next) => {
//     const queColeccion = req.params.colecciones;
//     const queID = req.params.id;
//     const nuevosDatos = req.body;

//     req.collection.update(
//         { _id: id(queID) },
//         {$set: nuevosDatos},
//         (err, resultado) => {
//             if (err) return next(err);

//             console.log(resultado);
//             res.json({
//                 result: "ok",
//                 coleccion: queColeccion,
//                 elemento: queID,
//                 resultado: resultado
//             });
//         }
//     );
// });

app.put('/api/:colecciones/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queID = req.params.id;
    const nuevosDatos = req.body;

    req.collection.update(
        { _id: id(queID) },
        {$set: nuevosDatos},
        (err, resultado) => {
            if (err) return next(err);

            console.log(resultado);
            res.json({
                result: "ok",
                coleccion: queColeccion,
                elemento: queID,
                resultado: resultado
            });
        }
    );
});

// app.delete('/api/:colecciones/:id', auth, (req, res, next) => {
//     const queColeccion = req.params.colecciones;
//     const queID = req.params.id;
//     //const queFecha = req.query.fecha; esto es un filtro
    

//     req.collection.remove(
//         { _id: id(queID) },
//         //{ $set: nuevosDatos },
//         { safe: true, multi: false },
//         (err, resultado) => {
//             if (err) return next(err);

//             console.log(resultado);
//             res.json({
//                 result: "ok",
//                 coleccion: queColeccion,
//                 resultado: resultado
//             });
//         }
//     );
// });

app.delete('/api/:colecciones/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queID = req.params.id;
    //const queFecha = req.query.fecha; esto es un filtro
    

    req.collection.remove(
        { _id: id(queID) },
        //{ $set: nuevosDatos },
        { safe: true, multi: false },
        (err, resultado) => {
            if (err) return next(err);

            console.log(resultado);
            res.json({
                result: "ok",
                coleccion: queColeccion,
                resultado: resultado
            });
        }
    );
});

// https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
//     console.log(`SEC WS API REST CRUD con DB ejecutándose en http://localhost:${port}/api/:colecciones/:id`);
// });


app.listen(port, () => {
    console.log(`WS API REST CRUD con DB ejecutándose en http://localhost:${port}/api/:colecciones/:id`);
});